/* automatically generated from main_window_ui.xml */
#ifdef __SUNPRO_C
#pragma align 4 (main_window_ui)
#endif
#ifdef __GNUC__
static const char main_window_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char main_window_ui[] =
#endif
{
  "<ui><menubar name=\"main-menu\"><menu action=\"file-menu\"><menuitem ac"
  "tion=\"open\"/><placeholder name=\"placeholder-open-recent\"/><separato"
  "r/><menuitem action=\"save-copy\"/><separator/><menuitem action=\"prope"
  "rties\"/><separator/><menuitem action=\"close\"/><menuitem action=\"qui"
  "t\"/></menu><menu action=\"edit-menu\"><menuitem action=\"copy-image\"/"
  "><separator/><menu action=\"open-with-menu\"><placeholder name=\"open-w"
  "ith-apps\" /></menu><separator/><menu action=\"sorting-menu\"><menuitem"
  " action=\"sort-filename\"/><menuitem action=\"sort-filetype\"/><menuite"
  "m action=\"sort-date\"/></menu><menuitem action=\"delete\"/><separator/"
  "><menuitem action=\"clear-private-data\"/><separator/><menuitem action="
  "\"preferences\"/></menu><menu action=\"view-menu\"><menuitem action=\"s"
  "how-toolbar\"/><menuitem action=\"show-statusbar\"/><menuitem action=\""
  "show-thumbnailbar\"/><menu action=\"thumbnailbar-position-menu\"><menui"
  "tem action=\"pos-left\"/><menuitem action=\"pos-right\"/><menuitem acti"
  "on=\"pos-top\"/><menuitem action=\"pos-bottom\"/></menu><menu action=\""
  "thumbnailbar-size-menu\"><menuitem action=\"size-very-small\"/><menuite"
  "m action=\"size-smaller\"/><menuitem action=\"size-small\"/><menuitem a"
  "ction=\"size-normal\"/><menuitem action=\"size-large\"/><menuitem actio"
  "n=\"size-larger\"/><menuitem action=\"size-very-large\"/></menu><separa"
  "tor/><menu action=\"zoom-menu\"><menuitem action=\"zoom-in\"/><menuitem"
  " action=\"zoom-out\"/><menuitem action=\"zoom-fit\"/><menuitem action=\""
  "zoom-100\"/><separator/><menu action=\"default-zoom-menu\"><menuitem ac"
  "tion=\"default-zoom-smart\"/><menuitem action=\"default-zoom-fit\"/><me"
  "nuitem action=\"default-zoom-100\"/></menu></menu><menu action=\"rotati"
  "on-menu\"><menuitem action=\"rotate-cw\"/><menuitem action=\"rotate-ccw"
  "\"/></menu><menu action=\"flip-menu\"><menuitem action=\"flip-horizonta"
  "lly\"/><menuitem action=\"flip-vertically\"/></menu><separator/><menuit"
  "em action=\"fullscreen\"/><separator/><menuitem action=\"set-as-wallpap"
  "er\"/></menu><menu action=\"go-menu\"><menuitem action=\"back\"/><menui"
  "tem action=\"forward\"/><menuitem action=\"first\"/><menuitem action=\""
  "last\"/><separator/><placeholder name=\"placeholder-slideshow\" /></men"
  "u><menu action=\"help-menu\"><menuitem action=\"contents\"/><menuitem a"
  "ction=\"about\"/></menu></menubar><popup name=\"navigation-toolbar-menu"
  "\"><menu action=\"position-menu\"><menuitem action=\"pos-left\"/><menui"
  "tem action=\"pos-right\"/><menuitem action=\"pos-top\"/><menuitem actio"
  "n=\"pos-bottom\"/></menu><menu action=\"size-menu\"><menuitem action=\""
  "size-very-small\"/><menuitem action=\"size-smaller\"/><menuitem action="
  "\"size-small\"/><menuitem action=\"size-normal\"/><menuitem action=\"si"
  "ze-large\"/><menuitem action=\"size-larger\"/><menuitem action=\"size-v"
  "ery-large\"/></menu></popup><popup name=\"image-viewer-menu\"><menuitem"
  " action=\"open\"/><menuitem action=\"properties\"/><menuitem action=\"c"
  "lose\"/><menuitem action=\"copy-image\"/><separator/><menu action=\"ope"
  "n-with-menu\"><placeholder name=\"open-with-apps\" /></menu><separator/"
  "><menuitem action=\"zoom-in\"/><menuitem action=\"zoom-out\"/><menuitem"
  " action=\"zoom-100\"/><menuitem action=\"zoom-fit\"/></popup><toolbar n"
  "ame=\"main-toolbar\"><toolitem action=\"open\"/><separator /><toolitem "
  "action=\"save-copy\"/><toolitem action=\"delete\"/><separator /><toolit"
  "em action=\"edit\"/><separator name=\"separator-1\"/><toolitem action=\""
  "back\"/><placeholder name=\"placeholder-slideshow\" /><toolitem action="
  "\"forward\"/><separator name=\"separator-2\"/><toolitem action=\"zoom-i"
  "n\"/><toolitem action=\"zoom-out\"/><toolitem action=\"zoom-100\"/><too"
  "litem action=\"zoom-fit\"/><separator /><placeholder name=\"placeholder"
  "-fullscreen\" /></toolbar></ui>"
};

static const unsigned main_window_ui_length = 3521u;

